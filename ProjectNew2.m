function ProjectNew2(fileName, predictCount)
    X=xlsread(fileName);
    X=X(:,1);
    N=length(X);
    t=1:N;
    display(N);
    Y=X(1:N-predictCount,:);
    lenY=length(Y);
     windows = [floor(N/2) floor(N/3)];
%      windows = [floor(N/3) floor(N/10)];
    wLen = length(windows);
    predictions = zeros(N,wLen);
    for i=1:wLen   
        eigVals = ssaspe(Y,windows(i));
        r = sum(eigVals(:)>1e-4);
        display(r)
        output = ssafor(Y,windows(i),r,predictCount);
        predictions(:,i) = output(:);
        delta = (output(lenY+1:N,:) - X(lenY+1:N,:))./X(lenY+1:N,:);
        display(norm(delta)/predictCount);
    end
    figure;
    set(gcf,'name','Initial time series X and Predictions')
    clf;
    subplot(3,1,1)
    plot(t(1:N),X,'b-');
    legend('Initial time series');
    subplot(3,1,2)
    plot(t(1:N),predictions(:,1),'r-');
    legend('Predicted with L1'); %sum of all reconstructed components    
    subplot(3,1,3)
    plot(t(1:N),predictions(:,2),'r-');
    legend('Predicted with L2'); %sum of all reconstructed components 
    display([X(lenY+1:N,:) predictions(lenY+1:N,1) predictions(lenY+1:N,2)])
    
    figure
    plot(t(lenY:N),predictions(lenY:N,2),'r-')
    hold on
    plot(t(lenY:N),X(lenY:N,1),'b-')
end

function [F] = ssafor(Y, L, R, M)
    %SSAFOR Returns reconstructed data plus M forecasted points using N components of the input series
    %   Inputs:
    %   Y - input data series column vector
    %   L - number of reconstructed components input data series to be decomposed to
    %       (note that L must be less the length of the input series Y)
    %   R - number of reconstructed components to be used for the forecast
    %   M - number of points to be forecasted
    %   Outputs:
    %   F - sum of the first R reconstructed components plus M points forecasted via R first reconstructed components
    %       (thus F has the dimension of (T + M), where T is the input series length)
    %   
    %   How to use:
    %   First of all get the data series spectrum, i.e. eigenvalues using 'ssaspe' function. Plot the
    %   eigenvalues on a log-scale graph to choose the number (R) of components
    %   containing valuable signal. Then you can effectively forecast the
    %   series with first R components.

    T = length(Y);
    K = T - L + 1;

    X = zeros(L,K);
    for i = 1:K
        X(:,i) = Y(i:i+L-1).';
    end

    [U,D] = eig(X*X.'); % #ok<NASGU>
%     display((D))
    U = flipdim(U,2); % To get vector sorted by eigen values in decreasing fashion
%     display((U))

    V = zeros(K,L);
    for i = 1:L
        V(:,i) = X.'*U(:,i);
    end

    Z = cell(L,1);
    Q = zeros(K,L);
    for i = 1:L
        Z{i} = U(:,i)*V(:,i).';
        Z{i} = flipdim(Z{i},2);
        for j = (-L + 1):(K-1)
           Q(L+j,i) = sum(diag(Z{i},j))/length(diag(Z{i},j));
        end
    end
    Q = flipdim(Q,1);
    A = zeros(L-1,1);
    for i = 1:R
        A = A + U(L,i)*U(1:L-1,i);
    end
    v = norm(U(L,1:R));
    A = A/(1-v^2);
    A = flipdim(A,1);
    G = sum(Q(:,1:R),2);
    F = [G.', zeros(1,M)].';

    for i = T+1:T+M
        for j = 1:L-1
            F(i) = F(i) + A(j)*F(i-j);
        end
    end
    F = F(T+1:T+M);
    F = [G.', F.'].';
end 

function [D] = ssaspe(Y, L)
    %   SSASPE Returns normalized SSA spectrum of the input series
    %   Inputs:
    %   Y - input data series column vector
    %   L - number of reconstructed components input data series to be decomposed to
    %       (note that L must be less than the length of the input series Y)
    %   Outputs:
    %   D - components' contributions to the input series in %

    T = length(Y);
    K = T - L + 1;

    X = zeros(L,K);
    for i = 1:K
        X(:,i) = Y(i:i+L-1).';
    end

    D = flipdim(eig(X*X.'),1);
    D = D/sum(D);
end